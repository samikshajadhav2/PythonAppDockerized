FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential -y
Run pip install pymysql
RUN pip install Flask
RUN pip install pyyaml
RUN apt-get install python3.6-dev libmysqlclient-dev -y
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
EXPOSE 8088
CMD ["app.py"]
